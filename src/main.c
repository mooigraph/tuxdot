
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 * Copyright mooigraph authors
 */

#include "config.h"

#include <stdio.h>
#include <string.h>

#include "options.h"
#include "draw.h"


int
main (int argc, char *argv[])
{
  FILE *f;

  /* */
  if (commandline_options (argc, argv))
    {
      return (1);
    }

  f = fopen (argv[1], "r");

  /* wait... work in progress */

  fclose (f);

  draw();

  return (0);
}

void update_statusline (char *text)
{
 return;
}


/* end */
