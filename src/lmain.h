
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

#ifndef LMAIN_H
#define LMAIN_H 1

/* number of levels in the drawing */
extern int number_of_layers;

/* */
extern int number_of_nodes;

/* */
extern int number_of_edges;

/* */
extern void parsed2layouter (void);

/* */
extern void mainl_clear (void);

/* */
extern void layout_graph (void);

/* */
extern void layouter2draw (void);

/* */
extern void layouter2draw_pos (void);

/* */
extern double xspreading;

/* */
extern double yspreading;

/* */
extern int set_preprocessor (const char *name);

/* */
extern const char *get_preprocessor (void);

/* */
extern int set_adjustweight (const char *name);

/* */
extern const char *get_adjustweight (void);

/* */
extern int set_heuristic (const char *name);

/* */
extern const char *get_heuristic (void);

/* */
extern int get_begin_crossings (void);

/* */
extern int get_end_crossings (void);

/* */
extern int xplace_mode;

/* */
extern char *is_valid_algo (char *algo);

/* */
extern char *is_valid_prealgo (char *prealgo);

/* */
extern void set_default_algo (void);

#endif

/* end */
