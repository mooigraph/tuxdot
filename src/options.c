
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#include "config.h"

#include <stdio.h>

#include "options.h"

/* if set print layouter debug --debug 0/1/2 or -D 0/1/2 */
int option_ldebug = 0;

int option_parsedebug = 0;

int option_gccbug = 0;

int option_testcode = 0;

int option_placedebug = 0;

int option_xyspread = 0;

int option_pud = 1;

int option_feprio = 0;


/* */
int
commandline_options (int argc, char *argv[])
{
  if (argc)
    {
    }
  if (argv)
    {
    }

  /* oke */
  return (0);
}

/* end */
