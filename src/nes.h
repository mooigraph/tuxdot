
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

#ifndef NES_H
#define NES_H 1

/* */
extern splay_tree sp_parsednl;

/* */
extern splay_tree sp_parsedel;

/* */
extern splay_tree sp_parsedsgl;

/* */
extern splay_tree sp_rootnl;

/* */
extern splay_tree sp_rootel;

/* */
extern splay_tree sp_summarynl;

/* */
extern splay_tree sp_wsummarynl;

/* */
extern splay_tree sp_virtualel;

/* */
extern splay_tree sp_virtualnl;

/* */
extern splay_tree sp_parsedwnl;

/* */
extern splay_tree sp_parsedwel;

/* */
extern splay_tree sp_parsedwsgl;

/* */
extern int maxx;

/* */
extern int maxy;

/* */
extern int uniq_node_number;

/* */
extern int n_drawing_nodes;

/* */
extern int n_drawing_edges;

/* */
extern int n_input_nodes;

/* */
extern int n_input_singlenodes;

/* */
extern int n_input_edges;

/* */
extern int n_input_revedges;

/* */
extern int n_input_selfedges;

/* */
extern int n_input_edgelabels;

/* */
extern int n_input_subgraphs;

/* */
extern int n_folded_cycles;

/* */
extern int n_folded_cycles_revedges;

/* */
extern struct drawn *drawnl;

/* */
extern struct drawn *drawnl_end;

/* */
extern struct drawe *drawel;

/* */
extern struct drawe *drawel_end;

/* */
extern struct dli **dlip;

/* */
extern int dli_nlevels;

/* */
extern void once_init (void);

/* */
extern void nes_clear (void);

/* */
extern void calculate_init (void);

/* */
extern void calculate (void);

/* */
extern void randomgraph (void);

/* */
extern void prepare4newdata (void);

/* */
extern void rootnl_clear (void);

/* */
extern void rootel_clear (void);

/* */
extern void virtualnl_clear (void);

/* */
extern void virtualel_clear (void);

/* */
extern void summarynl_clear (void);

/* */
extern void parsedwnl_clear (void);

/* */
extern void parsedwel_clear (void);

/* */
extern void drawnl_clear (void);

/* */
extern void drawel_clear (void);

/* */
extern void dli_clear (void);

/* */
extern struct unode *unode_new (char *name);

/* */
extern struct unode *udummynode_new (void);

/* */
extern struct unode *uedgelabel_new (char *label);

/* */
extern struct uedge *uedge_new (struct unode *fn, struct unode *tn);

/* */
extern struct usubg *usubg_new (char *name, struct usubg *rootedon);

/* */
extern void uedge_copy_attribs (struct uedge *from, struct uedge *to);

/* */
extern struct dln *dln_new (void);

/* */
extern struct drawn *drawn_new (void);

/* */
extern struct drawe *drawe_new (void);

/* */
extern struct ddn *ddn_new (void);

/* */
extern struct dde *dde_new (void);

/* */
extern struct dli *dli_new (void);

/* */
extern void virtualnl_add (struct unode *un);

/* */
extern void virtualel_add (struct uedge *ue);

/* */
extern void parsednl_add (struct unode *un);

/* */
extern void summarynl_add (struct unode *un);

/* */
extern void wsummarynl_add (struct unode *un);

/* */
extern void parsedwnl_add (struct unode *un);

/* */
extern void parsedel_add (struct uedge *ue);

/* */
extern void parsedwel_add (struct uedge *ue);

/* */
extern void parsedsgl_add (struct usubg *sg);

/* */
extern void clear_subg_nel (void);

/* */
extern void rebuild_subg_nel (void);

/* */
extern void unode_poe_add (struct unode *un, struct uedge *ue);

/* */
extern void rebuild_poe (void);

/* */
extern void drawnl_add (struct drawn *node);

/* */
extern void drawel_add (struct drawe *edge);

/* */
extern void dli_add_node (struct dli *lp, struct ddn *node);

/* */
extern void dli_add_edge (struct dli *lp, struct dde *edge);

/* */
extern void input_stats (void);

#endif

/* end */
