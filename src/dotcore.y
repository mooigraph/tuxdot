
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 * Copyright mooigraph authors
 */

/*
 * this grammar is intentionally not compatible with graphviz.
 * subset of dot language and not supported is:
 * "a"[foo] (foo is set to true, never used)
 * "a"[foo=bar][baz=boo] (never used)
 * subgraph "baz"; (empty subgraph, never used)
 * "a"+"b" (concat strings is never used)
 */

%{

#include "config.h"

#include <stdio.h>

#include "mem.h"
#include "uniqstring.h"

extern int dotlex(void);
extern char *currentfile;
extern int dotlex_lineno(void);

char *lastnum=NULL;
char *laststring=NULL;
char *lastid=NULL;
char *lasthtml=NULL;
char *lastchar=NULL;

char *parser_error_string=NULL;

int dotcorelex (void)
{
  return (dotlex());
}

struct ast
{
  void *data;
  struct ast *arg;
  struct li *next;
};


void yyerror (const char *s)
{
  fprintf(stderr,"yyerror: %s file %s line %d\n",s,currentfile,dotlex_lineno());
  fflush (stderr);
  return;
}

%}

/* this is a glr parser, not a yacc parser */
%glr-parser

%union { char *s; int subg; struct li *l; }

%token DOT_GRAPH	/* 'graph' */
%token DOT_NODE		/* 'node' */
%token DOT_EDGE		/* 'edge' */
%token DOT_SUBGRAPH	/* 'subgraph' */
%token DOT_DEDGE	/* '->' */
%token DOT_UEDGE	/* '--' */
%token DOT_BRACKETO	/* '[' */
%token DOT_BRACKETC	/* ']' */
%token DOT_COLON	/* ':' */
%token DOT_SEMIC	/* ';' */
%token DOT_COMMA	/* ',' */
%token DOT_BC		/* '{' */
%token DOT_BO		/* '}' */
%token DOT_IS		/* '=' */
%token <s> DOT_ID	/* identifier */
%token <s> DOT_NUM	/* 123.45 */
%token <s> DOT_STRING	/* "string" */
%token <s> DOT_CHAR	/* 'c' */
%token DOT_HTML		/* <<string>> not used here */

%token DOT_DIGRAPH	/* 'digraph' not used here */
%token DOT_STRICT	/* 'strict' not used here */
%token DOT_UTF8BOM	/* utf8 code not used here */
%token DOT_PLUS		/* '+' not used here */
%start dotcore

%%


/* statements can be empty */
dotcore:
   DOT_BO statements DOT_BC { /* $2 if NULL empty graph but valid */ }
 ;

/* statements */
statements :
   /* empty */ { /* $$=NULL */ }
 | statements dotstatement { }
 ;

/* node, edge, attribute or subgraph */
dotstatement :
   nodename maybe_attributes maybe_semicolon { printf("node st\n"); /* $1 $2 */ }
 | dotedge maybe_semicolon { printf("edge st\n"); /* $1 */ }
 | attr_dotstatement maybe_semicolon { printf("attr st\n");  /* $1 */ }
 | subgraph maybe_semicolon { printf("subg st\n");  /* $1 */ }
 ;

/* in a attribute list */
in_attributes :
   /* empty */ { /* $$=NULL */ }
 | id DOT_IS id maybe_sep in_attributes { printf("attr st\n"); /* $1 $3 */ }
 | id DOT_IS DOT_HTML maybe_sep in_attributes { printf("attr st\n"); /* $1 $3 */ }
 ;

/* id=id, or id=id; or id=id<space> */
maybe_sep :
   /* empty */
 | DOT_COMMA
 | DOT_SEMIC
 ;

/* maybe a list of attributes, not supported [][] */
maybe_attributes :
   /* empty */ { /* $$=NULL */ }
 | DOT_BRACKETO in_attributes DOT_BRACKETC { /* $$=$2 */ }
 ;

/* attribute for graph[], node[], edge[] or in rootgraph */
attr_dotstatement :
   DOT_GRAPH DOT_BRACKETO in_attributes DOT_BRACKETC { /* $3 */ }
 | DOT_NODE DOT_BRACKETO in_attributes DOT_BRACKETC { /* $3 */ }
 | DOT_EDGE DOT_BRACKETO in_attributes DOT_BRACKETC { /* $3 */ }
 | id DOT_IS id { /* graph $1 $3 */ }
 ;

/* optional ';' after statement */
maybe_semicolon:
   /* empty */
 | DOT_SEMIC
 ;

/* node id with optional compass points */
nodename :
   id { /* $1,0,0 */ }
 | id DOT_COLON id { /* $1,$2,0 */ }
 | id DOT_COLON id DOT_COLON id { /* $1,$2,$3 */ }
 ;

/* edge from node or subgraph */
dotedge :
   nodename { printf("b-edge\n"); } DOT_DEDGE righthandside { printf("m-edge\n"); }  maybe_attributes { printf("e-edge\n"); /* -> $1,$2,$3,$4 */ }
 | subgraph DOT_DEDGE righthandside maybe_attributes { /* -> $1,$2,$3,$4 */ }
 | nodename DOT_UEDGE righthandside maybe_attributes { /* -- $1,$2,$3,$4 */ }
 | subgraph DOT_UEDGE righthandside maybe_attributes { /* -- $1,$2,$3,$4 */ }
 ;

/* right-hand side of edge */
righthandside :
   nodename { /* $1 */ }
 | subgraph { /* $$=$1 */ }
 | nodename DOT_DEDGE righthandside { /* -> 1 2 3 */ }
 | subgraph DOT_DEDGE righthandside { /* -> 1 2 3 */ }
 | nodename DOT_UEDGE righthandside { /* -- 1 2 3 */ }
 | subgraph DOT_UEDGE righthandside { /* -- 1 2 3 */ }
 ;

/* subgraph statement with optional name */
subgraph :
   DOT_SUBGRAPH id dotcore { /* $2,$3 if id starts with cluster mark it */ }
 | DOT_SUBGRAPH dotcore { /* 0,$2 */ }
 | dotcore {/* 0 $1 */ }
 ;

/* ident and not supported "a"+"b" */
id :
   DOT_ID { }
 | DOT_NUM { }
 | DOT_CHAR { }
 | DOT_STRING { }
 ;

%%


/* init and set debug level */
void dotcoreparse_init (int i)
{
  yydebug = i;
  return;
}

/* end */
