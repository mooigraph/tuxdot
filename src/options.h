
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

#ifndef OPTIONS_H
#define OPTIONS_H 1

/* */
extern int commandline_options (int argc, char *argv[]);

/* if set print layouter debug --debug 0/1/2 or -D 0/1/2 */
extern int option_ldebug;

extern int option_parsedebug;

extern int option_gccbug;

extern int option_testcode;

extern int option_placedebug;

extern int option_xyspread;

extern int option_pud;

extern int option_feprio;

#endif

/* end */
