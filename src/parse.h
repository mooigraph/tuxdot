
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 *
 */

#ifndef PARSE_H
#define PARSE_H 1

/* tys parser keyword tokens */
#define TYS_EDGE 257
#define TYS_NODE 258
#define TYS_SETTINGS 259
#define TYS_SUBGRAPH 260
#define TYS_STRING 261
#define TYS_VOID 262
#define TYS_GRAPH 263
#define TYS_LABEL 264
#define TYS_COLOR 265
#define TYS_STRICT 266
#define TYS_DIGRAPH 267
#define TYS_STYLE 268
#define TYS_FOLDED 269
#define TYS_TEXTCOLOR 271
#define TYS_TEXTSLANT 272
#define TYS_TEXTWEIGHT 273
#define TYS_FONTSIZE 274
#define TYS_FONTNAME 275
#define TYS_SHAPE 276
#define TYS_THICKNESS 277
#define TYS_ARROWS 278
#define TYS_URL 279
#define TYS_UTF8BOM 280
#define TYS_CERROR 281		/* error at c comment */

/* dot language */
#define DOT_UTF8BOM 500
#define DOT_STRING 501
#define DOT_ID 502
#define DOT_NUM 503
#define DOT_CHAR 504
#define DOT_HTML 505
#define DOT_DEDGE 506		/* -> */
#define DOT_UEDGE 507		/* -- */
#define DOT_BO 508		/* brace open { */
#define DOT_BC 509		/* brace close } */
#define DOT_COLON 510		/* : */
#define DOT_BRACKETO 512	/* [ */
#define DOT_BRACKETC 513	/* ] */
#define DOT_COMMA 514		/* , */
#define DOT_IS 515		/* = */
#define DOT_SEMIC 516		/* ; */
#define DOT_CERROR 517		/* error at c comment */
#define DOT_PLUS 518		/* + */
#define DOT_STRICT 519		/* strict */
#define DOT_GRAPH 520		/* graph */
#define DOT_DIGRAPH 521		/* digraph */
#define DOT_SUBGRAPH 522	/* subgraph */
#define DOT_NODE 523		/* node */
#define DOT_EDGE 524		/* edge */

/* */
extern int parsedformat;
extern char *parser_error_string;

/* tys lexer */
extern void tyslexreset (void);
extern void tyslexinit (FILE * f, int tysdebug);
extern int tyslex_lineno (void);

/* dot */
extern int get_dotlex_lineno (void);
extern void parsedot_clear (void);
extern int parsedot (char *fname, FILE * fstream);

/* in dot lexer */
extern int dotlex (void);
extern void dotlexreset (void);
extern void dotlexinit (FILE * f, char *fname, int dotdebug);
extern int dotlex_lineno (void);
extern int dotlex_max_strlen (void);

/* parsetys.c */
extern void parse_clear (void);
extern void parsetys_clear (void);
extern void parse (char *filename);
extern char *laststring;
extern char *lastid;
extern char *lastnum;
extern char *lasthtml;
extern char *lastchar;

#endif

/* end */

