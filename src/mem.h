
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#ifndef MEM_H
#define MEM_H 1

/* malloc wrapper */
extern void *mymalloc (size_t n, const char *func, int line);

/* free wrapper */
extern void myfree (void *ptr, const char *func, int line);

/* realloc wrapper */
extern void *myrealloc (void *ptr, size_t size, const char *func, int line);

/* statistics */
extern void mymemstats (void);

/* memory check */
extern void myfinalmemcheck (void);

/* how many times malloc() is called */
extern unsigned long long ntotalmallocs(void);

/* how many times free() is called */
extern unsigned long long ntotalfree(void);

/* how much memory is in use */
extern unsigned long long ntotalmemuse(void);

#endif

/* end */
